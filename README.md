# Hypothesys BibLaTeX literature database

This is a BibLaTeX database for storage of references that I cite(d) or that I have read. The primary storage for the database is in a private MySQL database which is accessed via JabRef and synced to disk automatically.
