#!/usr/bin/env sh

# Synchronize this directory to ark.bi.up.ac.za
sudo vpnc
rsync -aHubv * --exclude '*.bak' --exclude 'README.md' --exclude '*~' equus@ark.bi.up.ac.za:/var/www/html/literature
